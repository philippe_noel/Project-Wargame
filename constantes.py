import units

RATIO = 32  # grossissement de la taille
taille_sprite = 16

TAILLE_FENETRE = [20, 20]


spriteDictUnit = {"Soldier" : [114, 118, 121, 128, 135],
              "Archer" : [112, 104, 119, 126, 133],
              "Lancer" : [113, 111, 120, 127, 134],
              "Balist" : [116, 132, 123, 130, 137],
              "Rider" : [115, 125, 122, 129, 136],
              "Ram" : [117, 139, 124, 131, 138],
              "PeonH" : [247, 257, 254, 261, 263],
              "PeonF" : [248, 264, 255, 262, 226]
              }

def getReverseDict(myDict):
    reverseDict = dict()
    for key, value in myDict.items():
        for element in value:
            reverseDict[element] = key
    return reverseDict

# Permet de créer un tableau de Hash inversé
reverseSpriteDictUnit = getReverseDict(spriteDictUnit)

# Permet de récupérer les informations croisés de spriteDict: indice 0 type (e.g "soldier", "rider", etc...) et indice 1 :le numéro du joeur associé
def getInformationFromSpriteDict(spriteIndex, reverseDict, spriteDict) :
    return list([reverseDict[spriteIndex], spriteDict[reverseDict[spriteIndex]].index(spriteIndex)])

# Permet de créer la classe associé au spriteIndex grâce au getInformationFromSpriteDict et au coord associées
def CreateUnitFromSpriteIndex(x, y, spriteIndex):
    if spriteIndex != -1:
        UnitInformation = getInformationFromSpriteDict(spriteIndex, reverseSpriteDictUnit, spriteDictUnit)
        MyClass = eval("units." + UnitInformation[0])
        newUnit = MyClass(x, y, UnitInformation[1])
        return newUnit
    else:
        return None
