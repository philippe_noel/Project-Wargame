



# Creation de la table des sprites - A passer en attribut d'une classe supérieure
# Bleue, Gris, Rouge, Jaune, Vert
spriteDictBatiment = {"Maison" : [7, 8, 9, 14, 15, 16, 17, 21],
              "Tour" : [25, 22, 23, 26, 24],
              "Tente" : [39, 37, 40, 38],
              "Balist" : [116, 132, 123, 130, 137],
              "Rider" : [115, 125, 122, 129, 136],
              "Ram" : [117, 139, 124, 131, 138],
              "PeonH" : [247, 257, 254, 261, 263],
              "PeonF" : [248, 264, 255, 262, 226]
              }
# Permet de créer un tableau de Hash inversé
def getReverseDict(myDict):
    reverseDict = dict()
    for key, value in myDict.items():
        for element in value :
            reverseDict[element] = key
    return reverseDict



reverseSpriteDict = getReverseDict(spriteDict)



# Permet de récupérer les informations croisés de spriteDict: indice 0 type (e.g "soldier", "rider", etc...) et indice 1 :le numéro du joeur associé
def getInformationFromSpriteDict(spriteIndex, reverseDict, spriteDict) :
    return list([reverseDict[spriteIndex], spriteDict[reverseDict[spriteIndex]].index(spriteIndex)])

# Permet de créer la classe associé au spriteIndex grâce au getInformationFromSpriteDict et au coord associées
def CreateUnitFromSpriteIndex(x, y, spriteIndex):
    if spriteIndex != -1:
        UnitInformation = getInformationFromSpriteDict(spriteIndex, reverseSpriteDict, spriteDict)
        MyClass = eval(UnitInformation[0])
        newUnit = MyClass(x, y, UnitInformation[1])
        return newUnit
    else :
        return None


# Creation de la matrice associée

def CreateMatrixOfObjectFromSpriteMatrix(spriteMatrix, nLigne, mCol) :
    newMatrix = [[None for x in range(mCol)] for y in range(nLigne)]

    for ligne in range(nLigne):
        for colonne in range(mCol):
            newMatrix[ligne][colonne] = CreateUnitFromSpriteIndex(ligne, colonne, spriteMatrix[ligne][colonne])
    return newMatrix




class Unit:
    """
     Definition des fonctions pour les unités
     """
    def __init__(self, x, y, joueur, idUnit):
        ''' Constructeur de base'''
        self.coord = [x, y]
        self.id = idUnit
        self.hp = 10
        self.p_walk = None
        self.p_attack = None
        self.p_def = None
        self.attck_range = [1, 1]
        self.move_attck = True
        self.can_move = 1
        self.can_attack = 1
        self.joueur = joueur
        self.spriteCoord = self.convert_indice(spriteDict[self.id][self.joueur])

    def convert_indice(self, indice):
        col = indice % 7
        ligne = indice / 7
        return col, int(ligne)

    def get_spriteCoord(self):
        return self.spriteCoord

    def get_joueur(self) :
        return self.joueur

    def get_coord(self):
        return self.coord

    def get_hp(self):
        return self.hp

    def get_p_walk(self):
        return self.p_walk

    def get_p_attack(self):
        return self.p_attack

    def get_p_def(self):
        return self.p_def

    def get_attck_range(self):
        return self.attck_range

    def get_move_attck(self):
        return self.move_attck

    def id_unit(self):
        if id == 0:
            self.destroy()

    def change_hp(self, damage):
        self.hp -= damage

    def change_coord(self, nx, ny):
        self.coord = [nx, ny]


class Soldier(Unit):
    ''' Definition du soldat'''
    def __init__(self, x, y, joueur):
        super().__init__(x, y, joueur, 'Soldier')
        self.p_walk = 4
        self.p_attack = 2
        self.p_def = 2

class Lancer(Unit):
    ''' Definition du lancier'''
    def __init__(self, x, y, joueur):
        super().__init__(x, y, joueur, 'Lancer')
        self.p_walk = 4
        self.p_attack = 2
        self.p_def = 2


class Archer(Unit):
    ''' Definition de l'archer'''
    def __init__(self, x, y, joueur):
        super().__init__(x, y, joueur, 'Archer')
        self.p_walk = 3
        self.p_attack = 1
        self.p_def = 1
        self.attck_range = [1, 2]


class Rider(Unit):
    ''' Definition du cavalier'''
    def __init__(self, x, y, joueur):
        super().__init__(x, y, joueur, 'Rider')
        self.p_walk = 6
        self.p_attack = 5
        self.p_def = 3


class Balist(Unit):
    ''' Definition de la baliste'''
    def __init__(self, x, y, joueur):
        super().__init__(x, y, joueur, 'Balist')
        self.p_walk = 2
        self.p_attack = 5
        self.p_def = 2
        self.p_attack = [1, 3]
        self.move_attck = False

class Ram(Unit):
    ''' Definition du lancier'''
    def __init__(self, x, y, joueur):
        super().__init__(x, y, joueur, 'Ram')
        self.p_walk = 4
        self.p_attack = 12
        self.p_def = 1

class PeonH(Unit):
    ''' Definition du PeonH'''
    def __init__(self, x, y, joueur):
        super().__init__(x, y, joueur, 'PeonH')
        self.p_walk = 2
        self.p_attack = 0
        self.p_def = 1

class PeonF(Unit):
    ''' Definition du PeonF'''
    def __init__(self, x, y, joueur):
        super().__init__(x, y, joueur, 'PeonF')
        self.p_walk = 2
        self.p_attack = 0
        self.p_def = 1



def carac(classe):
    print('Unit : ', classe.id)
    print('Hp points : ', classe.hp)
    print('Attck p : ', classe.p_attack)
    print('defense p : ', classe.p_def)
    print('deplacement p : ', classe.p_walk)
    print('joueur : ', classe.joueur)
    print('spriteCoord : ', classe.spriteCoord)
    print('Coord : ', classe.coord)


def modif(classe):
    classe.hp = 114


if __name__ == '__main__':
    sol1 = Soldier(5, 8, 1)
    cav = Rider(1, 2, 0)
    archer = Archer(5, 6, 0)
    balist = Balist(8, 9, 1)
    lanc = Lancer(10, 12, 2)
    ram = Ram(12, 15, 3)

    cc = CreateUnitFromSpriteIndex(20 ,40, 137)
    carac(cc)
    tt = CreateUnitFromSpriteIndex(3 ,8, 262)
    carac(tt)
