#Creation de la table des sprites - A passer en attribut d'une classe supérieure
# Bleue, Gris, Rouge, Jaune, Vert
spriteDict = {"Soldier" : [114, 118, 121, 128, 135],
              "Archer" : [112, 104, 119, 126, 133],
              "Lancer" : [113, 111, 120, 127, 134],
              "Balist" : [116, 132, 123, 130, 137],
              "Rider" : [115, 125, 122, 129, 136],
              "Ram" : [117, 139, 124, 131, 138],
              "PeonH" : [247, 257, 254, 261, 263],
              "PeonF" : [248, 264, 255, 262, 226]
              }
# Permet de créer un tableau de Hash inversé
def getReverseDict(myDict):
    reverseDict = dict()
    for key, value in myDict.items():
        for element in value:
            reverseDict[element] = key
    return reverseDict



reverseSpriteDict = getReverseDict(spriteDict)
    


# Permet de récupérer les informations croisés de spriteDict: indice 0 type (e.g "soldier", "rider", etc...) et indice 1 :le numéro du joeur associé
def getInformationFromSpriteDict(spriteIndex, reverseDict, spriteDict) :
    return list([reverseDict[spriteIndex], spriteDict[reverseDict[spriteIndex]].index(spriteIndex)])

# Permet de créer la classe associé au spriteIndex grâce au getInformationFromSpriteDict et au coord associées
def CreateUnitFromSpriteIndex(x, y, spriteIndex):
    if spriteIndex != -1:
        UnitInformation = getInformationFromSpriteDict(spriteIndex, reverseSpriteDict, spriteDict)
        MyClass = eval(UnitInformation[0])
        newUnit = MyClass(x, y, UnitInformation[1])
        return newUnit
    else :
        return None
            

# Creation de la matrice associée

def CreateMatrixOfObjectFromSpriteMatrix(spriteMatrix, nLigne, mCol) :
    newMatrix = [[None for x in range(mCol)] for y in range(nLigne)]
    
    for ligne in range(nLigne):
        for colonne in range(mCol):
            newMatrix[ligne][colonne] = CreateUnitFromSpriteIndex(ligne, colonne, spriteMatrix[ligne][colonne])
    return newMatrix




class Unit:
    """
        Definition des fonctions pour les unités
    """
    def __init__(self, x, y, joueur, id_unit):
        """
            Constructeur de base de la classe Unit

            :param x: position en x sur la carte
            :type x: int
            :param y: position en y sur la carte
            :type y: int
            :param joueur: Joueur a qui cette unité appartient
            :type joueur: int
            :param id_unit: Non utilisé
            :type id_unit: str
        """
        self.coord = [x, y]
        self.id = id_unit
        self.hp = 10
        self.p_walk = None
        self.p_attack = None
        self.p_def = None
        self.attck_range = [1, 1]
        self.move_attck = True
        self.can_move = 1
        self.can_attack = 1
        self.joueur = joueur
        self.spriteCoord = self.convert_indice(spriteDict[self.id][self.joueur])

    def get_sprite_coord(self):
        """
        Return les coordonnées en x, y du sprite sur la tilesheet
        :return: [coordonnées en x, coordonnées en y]
        :rtype: tuple
        """
        return self.spriteCoord
        
    def get_joueur(self):
        """
        Return le numéro du joueur a qui appartient l'unité

        :return: int le numéro du joueur
        :rtype: int
        """
        return self.joueur

    def get_coord(self):
        """
        Return les coordonnées de l'unité sur la carte

        :return: liste contenant les coordonnées [x, y]
        :rtype: list
        """
        return self.coord

    def get_hp(self):
        """
        Return les points de vie de l'unité

        :return: les points de vies
        :rtype: int
        """
        return self.hp

    def get_p_walk(self):
        """
        Retourne les points de déplacement de l'unité

        :return: points de déplacement
        :rtype: int
        """
        return self.p_walk

    def get_p_attack(self):
        """
        Retourne les points d'attaque de l'unité

        :return: points d'attaque
        :rtype: int
        """
        return self.p_attack

    def get_p_def(self):
        """
        Retourne les points de défense de l'unité

        :return: points de défense
        :rtype: int
        """
        return self.p_def

    def get_attck_range(self):
        """
        Retourne une liste contenant la porté minimale et maximale de l'unité

        :return: [Porté min, porté max]
        :rtype: list
        """
        return self.attck_range

    def get_move_attck(self):
        """
        Retourne True si l'unité peut se déplacer et attaquer au même tour. Et False si dans un même tour, l'unité peut
        Soit attaquer, soit bouger.

        :return: True/False
        :rtype: bool
        """
        return self.move_attck

    def id_unit(self):
        """
        Retourne l'identifiant de l'unité ???
        TODO : Est ce que ça sert à quelque chose ?

        :return: Identifiant de l'unité
        :rtype: int
        """
        return self.id

    def change_hp(self, damage):
        """
            Modifie les points de vie de l'unité
            TODO : Supprimer l'unité si pdv <= 0
            :param damage: nombre de points de vie en moins de l'unité
            :type damage: int
        """
        self.hp -= damage

    def change_coord(self, nx, ny):
        """
        Modifie les coordonnées de l'unité sur la carte

        :param nx: correspond aux coordonnées x (lignes) sur la carte
        :type nx: int
        :param ny: correspond aux coordonnées y (colonnes) sur la carte
        :type ny: int
        """
        self.coord = [nx, ny]

    def convert_indice(self, indice):
        """
            converti un sprite voulu en sa position en colonne, ligne sur la tilesheet. Le tilesheet est composé
            de 7 colonnes pour 44 lignes.

            :param indice: un int -> indice du sprite à chercher
            :type indice: int
            :return: Une liste contenant la colonne et la ligne où se trouve le sprite. indice à partir de 0
            :rtype: list
        """
        col = indice % 7
        ligne = indice / 7
        return col, int(ligne)


class Soldier(Unit):
    """
        Constructeur de base de la classe Soldier hérite de Unit

        :param x: position en x sur la carte
        :param y: position en y sur la carte
        :param joueur: Joueur a qui cette unité appartient
    """
    def __init__(self, x, y, joueur):
        super().__init__(x, y, joueur, 'Soldier')
        self.p_walk = 4
        self.p_attack = 2
        self.p_def = 2


class Lancer(Unit):
    """
        Constructeur de base de la classe Lancer hérite de Unit

        :param x: position en x sur la carte
        :param y: position en y sur la carte
        :param joueur: Joueur a qui cette unité appartient
    """
    def __init__(self, x, y, joueur):
        super().__init__(x, y, joueur, 'Lancer')
        self.p_walk = 4
        self.p_attack = 2
        self.p_def = 2


class Archer(Unit):
    """
        Classe Archer, hérite de la classe Unit
    """
    def __init__(self, x, y, joueur):
        """
            Constructeur de base de la classe Archer hérite de Unit

            :param x: position en x sur la carte
            :param y: position en y sur la carte
            :param joueur: Joueur a qui cette unité appartient
        """
        super().__init__(x, y, joueur, 'Archer')
        self.p_walk = 3
        self.p_attack = 1
        self.p_def = 1
        self.attck_range = [1, 2]


class Rider(Unit):
    """
        Classe cavalier, hérite de la classe Unit
    """
    def __init__(self, x, y, joueur):
        """
            Constructeur de base de la classe Rider hérite de Unit

            :param x: position en x sur la carte
            :param y: position en y sur la carte
            :param joueur: Joueur a qui cette unité appartient
        """
        super().__init__(x, y, joueur, 'Rider')
        self.p_walk = 6
        self.p_attack = 5
        self.p_def = 3


class Balist(Unit):
    """
        Classe Baliste, hérite de la classe Unit
    """
    def __init__(self, x, y, joueur):
        """
            Constructeur de base de la classe Balist hérite de Unit

            :param x: position en x sur la carte
            :param y: position en y sur la carte
            :param joueur: Joueur a qui cette unité appartient
        """
        super().__init__(x, y, joueur, 'Balist')
        self.p_walk = 2
        self.p_attack = 5
        self.p_def = 2
        self.p_attack = [1, 3]
        self.move_attck = False


class Ram(Unit):
    """
        Classe Bélier, hérite de la classe Unit
    """
    def __init__(self, x, y, joueur):
        """
            Constructeur de base de la classe Ram hérite de Unit

            :param x: position en x sur la carte
            :param y: position en y sur la carte
            :param joueur: Joueur a qui cette unité appartient
        """
        super().__init__(x, y, joueur, 'Ram')
        self.p_walk = 4
        self.p_attack = 12
        self.p_def = 1


class PeonH(Unit):
    """
        Classe Villageois Humain, hérite de la classe Unit
    """
    def __init__(self, x, y, joueur):
        """
            Constructeur de base de la classe PeonH hérite de Unit

            :param x: position en x sur la carte
            :param y: position en y sur la carte
            :param joueur: Joueur a qui cette unité appartient
        """
        super().__init__(x, y, joueur, 'PeonH')
        self.p_walk = 2
        self.p_attack = 0
        self.p_def = 1


class PeonF(Unit):
    """
        Classe Villageois Femme, hérite de la classe Unit
    """
    def __init__(self, x, y, joueur):
        """
            Constructeur de base de la classe PeonF hérite de Unit

            :param x: position en x sur la carte
            :param y: position en y sur la carte
            :param joueur: Joueur a qui cette unité appartient
        """
        super().__init__(x, y, joueur, 'PeonF')
        self.p_walk = 2
        self.p_attack = 0
        self.p_def = 1


def carac(classe):
    print('Unit : ', classe.id)
    print('Hp points : ', classe.hp)
    print('Attck p : ', classe.p_attack)
    print('defense p : ', classe.p_def)
    print('deplacement p : ', classe.p_walk)
    print('joueur : ', classe.joueur)
    print('spriteCoord : ', classe.spriteCoord)
    print('Coord : ', classe.coord)



if __name__ == '__main__':
    sol1 = Soldier(5, 8, 1)
    cav = Rider(1, 2, 0)
    archer = Archer(5, 6, 0)
    balist = Balist(8, 9, 1)
    lanc = Lancer(10, 12, 2)
    ram = Ram(12, 15, 3)

    cc = CreateUnitFromSpriteIndex(20, 40, 137)
    carac(cc)
    tt = CreateUnitFromSpriteIndex(3, 8, 262)
    carac(tt)
