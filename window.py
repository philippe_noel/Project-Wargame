import pygame

import dependances.spritesheet as spritesheet
import constantes
import map
import GUI
import actions


class Affichage:

    """
        Classe permettant l'affichage des sprites sur la fenetre
    """

    def __init__(self, fenetre, map, ratio, taille_fenetre):
        """
            Constructeur de base de la classe Afficher

            :param fenetre: objet de la classe pygame.display où afficher le jeu
            :type fenetre: pygame.display
            :param map: carte à afficher
            :type map: map.MapWindow
            :param ratio: ratio du zoom. De base, utiliser un ratio de 32 (x2 sur la taille des tilesets)
            :type ratio: int
            :param taille_fenetre: Taille maximale de la fenetre de jeu en nombre de case [nb_ligne, nb_colonne]
            :type taille_fenetre: list
        """
        self.map = map  # type: map.MapWindow
        self.fenetre = fenetre  # type: pygame.display
        self.taille_fenetre_max = taille_fenetre
        self.tileset = spritesheet.spritesheet('Images/tileset.png')
        self.ratio = ratio
        self.pos_curseur = [0, 0]

        # case initiale de la fenetre, celle à afficher en haut à droite. Change pendant le scrolling
        self.case_principale_fenetre = [0, 0]

        #Curseur
        self.curseur = GUI.Curseur(self.map)  # type: GUI.Curseur
        self.afficher_curseur()

    def get_map(self):
        """
        Return l'objet map de la fenetre principale
        :return: l'objet Map de la fenetre
        :rtype: object
        """
        return self.map

    def get_curseur(self):
        """
        Return l'objet curseur de la fenetre principale
        :return: l'object Curseur de la fenetre
        :rtype: object
        """
        return self.curseur

    def afficher_carte_entiere(self):
        self.affichage_terrain()
        self.afficher_batiments()
        self.afficher_units()
        self.afficher_curseur()

    def affichage_terrain(self):
        """
            Affiche les 3 layers les plus bas de l'objet map (sous1, sous2 et terrain)
            TODO Ne pas afficher la matrice terrain ici et créer des objets à partir du terrain
        """
        taille_fenetre = self.taille_fenetre_max
        case_principale = self.case_principale_fenetre
        for matrice in [self.map.matrice_ss_couche2, self.map.matrice_ss_couche1, self.map.matrice_terrain]:
            pos_map_x = 0
            pos_map_y = 0
            for indice_1 in range(taille_fenetre[0]):
                pos_map_x = 0
                for indice_2 in range(taille_fenetre[1]):
                    x, y = self.convert_indice(matrice[indice_1 + case_principale[0]][indice_2+case_principale[1]])  # obtention des coordonnées des pixels pour le sprite
                    element_sprite = self.tileset.image_at((x * 16, y * 16, 16, 16), colorkey=(0, 0, 0))
                    self.fenetre.blit(pygame.transform.scale(element_sprite, (self.ratio, self.ratio)), (pos_map_x, pos_map_y))
                    pos_map_x += self.ratio
                pos_map_y += self.ratio

    def afficher_units(self):
        """
            Affiche la matrice unité de l'objet map
        """
        matrice = self.map.matrice_unit
        taille_fenetre = self.taille_fenetre_max
        case_principale = self.case_principale_fenetre
        pos_map_x = 0
        pos_map_y = 0
        for indice_1 in range(taille_fenetre[0]):
            pos_map_x = 0
            for indice_2 in range(taille_fenetre[1]):
                if matrice[indice_1+case_principale[0]][indice_2+case_principale[1]] != -1:
                    x, y = self.convert_indice(
                        matrice[indice_1+case_principale[0]][indice_2+case_principale[1]])  # obtention des coordonnées des pixels pour le sprite
                    element_sprite = self.tileset.image_at((x * 16, y * 16, 16, 16), colorkey=(0, 0, 0))
                    self.fenetre.blit(pygame.transform.scale(element_sprite, (self.ratio, self.ratio)),
                                      (pos_map_x, pos_map_y))
                pos_map_x += self.ratio
            pos_map_y += self.ratio

    def afficher_curseur(self):
        """
        Permet d'afficher le curseur sur la carte
        """
        indice = 297
        x, y = self.convert_indice(indice)
        element_sprite = self.tileset.image_at((x * 16, y * 16, 16, 16), colorkey=(0, 0, 0))
        self.fenetre.blit(pygame.transform.scale(element_sprite, (self.ratio, self.ratio)),
                          (self.curseur.get_position_x_fenetre()*self.ratio, self.curseur.get_position_y_fenetre()*self.ratio))
        pygame.display.flip()

        # Afficher informations du curseur

    def set_case_principale_fenetre(self, x, y):
        """
        Fixe la case principale de la fenetre (aka la caméra). Elle correspond à la case en haut à droite de la fenetre
        et sa position sur la carte de jeu qui peut etre plus grande que la taille de la fenetre.
        :param x: nb ligne à déplacer (en générale -1 ou 1)
        :type x: int
        :param y: nb colonne à déplacer (en générale -1 ou 1)
        :type y: int
        """
        self.case_principale_fenetre[1] += x
        self.case_principale_fenetre[0] += y
        self.afficher_carte_entiere()

    def afficher_batiments(self):
        """
            Affiche la matrice Batiment de l'objet map
        """
        matrice = self.map.matrice_batiment
        taille_fenetre = self.taille_fenetre_max
        case_principale = self.case_principale_fenetre
        pos_map_x = 0
        pos_map_y = 0
        for indice_1 in range(taille_fenetre[0]):
            pos_map_x = 0
            for indice_2 in range(taille_fenetre[1]):
                if matrice[indice_1+case_principale[0]][indice_2+case_principale[1]] != -1:
                    x, y = self.convert_indice(
                        matrice[indice_1+case_principale[0]][indice_2+case_principale[1]])  # obtention des coordonnées des pixels pour le sprite
                    element_sprite = self.tileset.image_at((x * 16, y * 16, 16, 16), colorkey=(0, 0, 0))
                    self.fenetre.blit(pygame.transform.scale(element_sprite, (self.ratio, self.ratio)),
                                  (pos_map_x, pos_map_y))
                pos_map_x += self.ratio
            pos_map_y += self.ratio

    def rafraichir_case(self, x, y):
        """
        Methode qui permet d'actualiser une case de la carte

        :param x: ligne a actualiser
        :type x: int
        :param y: colonne a actualiser
        :type y: int
        """
        indice_x = x
        indice_y = y
        case_principale = self.case_principale_fenetre
        position_x = x * self.ratio
        position_y = y * self.ratio
        lst_matrice = [self.map.matrice_ss_couche2,
                       self.map.matrice_ss_couche1,
                       self.map.matrice_terrain,
                       self.map.matrice_batiment,
                       self.map.matrice_unit]
        for matrice in lst_matrice:
            indice = matrice[indice_y+case_principale[0]][indice_x+case_principale[1]]
            if indice != -1:
                x, y = self.convert_indice(indice)
                element_sprite = self.tileset.image_at((x * 16, y * 16, 16, 16), colorkey=(0, 0, 0))
                self.fenetre.blit(pygame.transform.scale(element_sprite, (self.ratio, self.ratio)),
                                 (position_x, position_y))

    def convert_indice(self, indice):
        """
            converti un sprite voulu en sa position en colonne, ligne sur la tilesheet. Le tilesheet est composé
            de 7 colonnes pour 44 lignes.

            :param indice: un int -> indice du sprite à chercher
            :type indice: int
            :return: Une liste contenant la colonne et la ligne où se trouve le sprite. indice à partir de 0
            :rtype: list
        """
        col = indice % 7
        ligne = indice / 7
        return col, int(ligne)

