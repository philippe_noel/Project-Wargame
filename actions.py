import map
import GUI
import units

class Selection:

    def __init__(self, mapw, curseur):
        """

        :type mapw: map.MapWindow
        """
        self.map = mapw  # type: map.MapWindow
        self.curseur = curseur  # Object from Curseur class
        self.objet = None
        self.selected = False  # Tag pour savoir si une action est en cours
        self.pos_curseur_x_1 = None
        self.pos_curseur_y_1 = None

    def get_selected_tag(self):
        return self.selected

    def selection_is_true(self):
        curseur_x = self.curseur.get_position_x_map()
        curseur_y = self.curseur.get_position_y_map()
        self.pos_curseur_x_1 = curseur_x
        self.pos_curseur_y_1 = curseur_y
        objet_case = self.map.get_unit_from_case(curseur_x, curseur_y)
        objet_case: units.Unit
        if objet_case:
            self.objet = objet_case
            self.selected = True
            print("unite selectionnée")
        else:
            pass

    def deplacement(self):
        curseur_x = self.curseur.get_position_x_map()
        curseur_y = self.curseur.get_position_y_map()

        print("old", curseur_x, curseur_y)
        print("new", self.pos_curseur_x_1, self.pos_curseur_y_1)

        self.objet.change_coord(curseur_x, curseur_y)
        self.map.changer_coord_unit(self.pos_curseur_x_1, self.pos_curseur_y_1, curseur_x, curseur_y)
        return None


class OverlaySelection:
    def __init__(self, mapw, coord_x, coord_y):
        self.mapw = mapw
        self.coord_x = coord_x
        self.coord_y = coord_y
        self.unit = mapw.get_unit_from_case(self.coord_x, self.coord_y)
        self.limite_plus = self.unit.get_p_walk()
        self.dictionnaire = dict()
        self.graph = None

        # DEBUG
        self.limite_plus = 2
        print(self.limite_plus)

    def generation_graph(self):
        nb_sommet = (self.limite_plus*2+1)**2
        # Le nombre de sommet du graph
        lst_sommet = [x for x in range(nb_sommet)]
        print(len(lst_sommet))
        # Lien entre les sommets
        liens_sommet = dict()
        len_ligne = self.limite_plus * 2 + 1
        coord_carre = [-1, 1, -len_ligne, len_ligne]

        for ii in lst_sommet:
            liens_sommet[ii] = []
            for uu in coord_carre:
                print(ii, ii + uu)
                if 0 <= ii+uu <= nb_sommet -1:
                    liens_sommet[ii].append(lst_sommet[ii+uu])
        print(liens_sommet)

        self.graph = [Sommet(sommet, 1, liens_sommet[sommet]) for sommet in liens_sommet]

    def prim(self):
        possib = [-1 for _ in range((self.limite_plus*2+1)**2)]
        sommet_base = 12
        sommet_a_parcourir = []
        sommet_parcouru = []

        cout = 0

        #while continue == True:
        #   sommet = self.graph


        for ii in self.graph:
            pass

    def bfs(self):
        G = self.dictionnaire
        s = 12
        P, Q = {s: None},[s]
        v = 0
        while Q:
            u = Q.pop(0)
            for v in G[u]:
                if v in P: continue
            P[v] = u
            Q.append(v)
        print(P)


class Sommet:
    def __init__(self, nom, attribut_terrain, list_suivant):
        self.nom = nom
        self.attribut_terrain = attribut_terrain
        self.list_suivant = list_suivant


if __name__ == '__main__':
    import map

    ob = map.MapWindow('maps/map2.tmx')

    tutu = OverlaySelection(ob, 8, 7)
    tutu.generation_graph()
    tutu.bfs()