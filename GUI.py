import constantes

class Curseur:
    """
    Classe permettant de créer le curseur à déplacer sur la carte
    """

    def __init__(self, map_window):
        """
        Créer un objet de type Curseur permettant de selectionner les unités et les actions
        :param map_window: Object de type Map
        :type map_window: map.MapWindow
        """
        self.position_x_map = 0
        self.position_y_map = 0
        self.position_x_fenetre = 0
        self.position_y_fenetre = 0
        self.limite_fenetre = constantes.TAILLE_FENETRE
        self.taille_map = map_window.get_taille_map()

    def get_position_x_map(self):
        return self.position_x_map

    def get_position_y_map(self):
        return self.position_y_map

    def get_position_x_fenetre(self):
        return self.position_x_fenetre

    def get_position_y_fenetre(self):
        return self.position_y_fenetre

    def deplacer_x(self, x):
        """
        Deplace le curseur sur l'axe des X ( colonnes)

        :param x: nombre de colonne à déplacer (négatif ou positif)
        :return: True s'il doit y avoir scrolling
                 false dans le cas contraire
        :rtype: bool
        """
        self.position_x_map += x
        if x > 0:
            if self.position_x_map > self.taille_map[1] - 1 :
                self.position_x_map = self.taille_map[1] - 1
                return False
            else:
                self.position_x_fenetre += x
                if self.position_x_fenetre > self.limite_fenetre[1] - 1:
                    self.position_x_fenetre = self.limite_fenetre[1] - 1
                    return True
        if x < 0:
            if self.position_x_map < 0:
                self.position_x_map = 0
                return False
            else:
                self.position_x_fenetre += x
                if self.position_x_fenetre < 0:
                    self.position_x_fenetre = 0
                    return True

    def deplacer_y(self, y):
        """
        Deplace le curseur sur l'axe des Y ( lignes)

        :param y: nombre de lignes à déplacer (négatif ou positif)
        :return: True s'il doit y avoir scrolling
                 false dans le cas contraire
        :rtype: bool
        """
        self.position_y_map += y

        if y > 0:
            if self.position_y_map > self.taille_map[0] - 1:
                self.position_y_map = self.taille_map[0] - 1
                return False
            else:
                self.position_y_fenetre += y
                if self.position_y_fenetre > self.limite_fenetre[0] - 1:
                    self.position_y_fenetre = self.limite_fenetre[0] - 1
                    return True

        if y < 0:
            if self.position_y_map < 0:
                self.position_y_map = 0
                return False
            else:
                self.position_y_fenetre += y
                if self.position_y_fenetre < 0:
                    self.position_y_fenetre = 0
                    return True


class InfoCurseur:
    """
    Affichage des informations sous le curseur
    """

    def __init__(self, curseur, map_window):
        self.curseur = curseur
        self.map_window = map_window

    def afficher_info_curseur(self):
        x, y = self.curseur.get_position_x_map(), self.curseur.get_position_y_map()
        terrain = self.map_window.matrice_terrain[x][y]
        unit = self.map_window.matrice_unit[x][y]
        batiment = self.map_window.matrice_batiment[x][y]

if __name__ == '__main__':
    import map
    ob = map.MapWindow('maps/map1.tmx')
    curseur = Curseur(ob)
    informations = InfoCurseur(curseur, ob)
    informations.afficher_info_curseur()
    curseur.deplacer_x(4)
    curseur.deplacer_y(6)
    informations.afficher_info_curseur()