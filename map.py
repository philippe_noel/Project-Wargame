from lxml import etree

import units
import constantes


class CreationElementMap:
    """
    Classe qui lit un fichier texte pour en extraire les informations et créer un objet map.MapWindow
    """

    def __init__(self, fichier_map):
        self.fichier = fichier_map
        self.taille_map = self.__taille_map()  # liste [nb ligne, nb colonne]
        self.matrice_unit = self.parse_map('Units')
        self.matrice_batiment = self.parse_map('Batiments')
        self.matrice_terrain = self.parse_map('Terrains')
        self.matrice_ss_couche1 = self.parse_map('sous1')
        self.matrice_ss_couche2 = self.parse_map('sous2')

        self.matrice_objets_unit = self.creer_matrice_objets_from_matrice_sprite()

    def __taille_map(self):
        """
        Determine la taille de la carte en se basant sur les informations du fichier de la map

        :return: Retourne une liste contenant le nombre de ligne et le nombre de colonne [nb_ligne, nb_col]
        :rtype: list
        """
        tree = etree.parse(self.fichier)
        for data in tree.xpath("/map"):
            return [int(data.get('height')), int(data.get('width'))]

    def parse_map(self, attribut):
        """
        Parse le fichier de la map et recherche l'attribut et génère une matrice contenant les identifiants du sprites.
        Un identifiant == -1 signifie qu'il n'y a pas de sprite
        L'attribut possible correspond aux différents layers lors de la création
        de la map avec le logiciel tiled. Les attributs possibles sont :
        Units
        Batiments
        Terrains
        sous1
        sous2

        :param attribut: layers a parser (voir doc juste au dessus)
        :type attribut: str
        :return: une matrice avec les identifiants du sprite (liste[][])
        :rtype: list
        """

        matrice = [[-1 for _ in range(self.taille_map[1])] for _ in range(self.taille_map[0])]

        tree = etree.parse(self.fichier)
        for data in tree.xpath('/map/layer'):
            if data.get('name') == attribut:
                for child in data:
                    child = child.text.replace('\n', '').split(',')
        x = 0
        y = 0
        for element in child:
            matrice[y][x] = int(element) - 1
            x += 1
            if x == self.taille_map[1]:
                y += 1
                x = 0

        return matrice

    def creer_matrice_objets_from_matrice_sprite(self):
        """
        Créer une matrice contenant des objects en fonction de la matrice entrée en paramètre.
        La matrice est de la taille de la carte et permet de positionner les unités sur la carte.

        :param matrice_sprite: matrice à parser pour récupérer les attributs
        :type matrice_sprite: list
        :return: la matrice contenant les objets  (liste [][])
        :rtype: list
        """

        new_matrix = [[None for _ in range(self.taille_map[1])] for _ in range(self.taille_map[0])]

        for ligne in range(self.taille_map[0]):
            for colonne in range(self.taille_map[1]):
                new_matrix[ligne][colonne] = constantes.CreateUnitFromSpriteIndex(ligne,
                                                                                  colonne,
                                                                                  self.matrice_unit[ligne][colonne])

        return new_matrix

    def get_map(self):
        """
        Retourne la nouvelle map crée
        :return: map.MapWindow
        """

        new_map = MapWindow(self.taille_map, self.matrice_unit, self.matrice_batiment, self.matrice_terrain,
                            self.matrice_ss_couche1, self.matrice_ss_couche2, self.matrice_objets_unit,
                            None, None)
        return new_map


class MapWindow:
    """
    Classe qui contient les informations pour une map et permet la génération de celle-ci
    La map est caractérisé par un fichier, une taille et un ensemble de matrice
    """

    def __init__(self, taille_carte, sprites_units, sprites_batiments, sprites_terrain, sprites_ss_couche1,
                 sprites_ss_couche2, objet_units, objet_batiment, objet_terrain):

        self.taille_map = taille_carte
        self.sprite_unit = sprites_units
        self.sprite_batiment = sprites_batiments
        self.sprite_terrain = sprites_terrain
        self.sprites_ss_couche1 = sprites_ss_couche1
        self.sprites_ss_couche2 = sprites_ss_couche2
        self.objet_units = objet_units
        self.objet_batiment = objet_batiment
        self.objet_terrain = objet_terrain

    def get_taille_map(self):
        """
        return la taille de la map
        :return: [nb_ligne, nb_col]
        """
        return self.taille_map

    def get_unit_from_case(self, ligne, col):
        """
        Retourne l'unité de la case selectionnée
        :param ligne:
        :param col:
        :return:
        """
        return self.matrice_objets_unit[col][ligne]

    def changer_coord_unit(self, old_x, old_y, new_x, new_y):
        """
        Modifie les coordonnées d'une unité sur la carte. Rentrer les anciennes et les nouvelles
        TODO : Virer et le mettre dans actions -> déplacement
        :param old_x:
        :param old_y:
        :param new_x:
        :param new_y:
        :return:
        """
        self.matrice_unit[new_y][new_x] = self.matrice_unit[old_y][old_x]
        self.matrice_unit[old_y][old_x] = -1
        self.matrice_objets_unit[new_y][new_x] = self.matrice_objets_unit[old_y][old_x]
        self.matrice_objets_unit[old_y][old_x] = None


if __name__ == '__main__':
    ob = CreationElementMap('maps/map1.tmx')
    uu = ob.get_map()
    print(uu.get_taille_map())




