import pygame

import constantes as cst
import map
import actions
import window


if __name__ == '__main__':
    pygame.init()

    # Ouverture de la fenêtre Pygame
    fenetre = pygame.display.set_mode((cst.RATIO*cst.TAILLE_FENETRE[0]+4*cst.RATIO, cst.RATIO*cst.TAILLE_FENETRE[1]))
    map1 = map.MapWindow('maps/map2.tmx')

    main_window = window.Affichage(fenetre, map1, cst.RATIO, [cst.TAILLE_FENETRE[0], cst.TAILLE_FENETRE[1]])

    main_window.afficher_carte_entiere()
    pygame.display.flip()

    action = actions.Selection(main_window.get_map(), main_window.get_curseur())

    # BOUCLE INFINIE
    while True:
        # Limitation de vitesse de la boucle
        # 30 frames par secondes suffisent
        pygame.time.Clock().tick(30)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_UP:
                    main_window.rafraichir_case(main_window.curseur.get_position_x_fenetre(), main_window.curseur.get_position_y_fenetre())
                    if main_window.curseur.deplacer_y(-1):
                        main_window.set_case_principale_fenetre(0, -1)
                        main_window.afficher_carte_entiere()
                    else:
                        main_window.afficher_curseur()

                if event.key == pygame.K_DOWN:
                    main_window.rafraichir_case(main_window.curseur.get_position_x_fenetre(), main_window.curseur.get_position_y_fenetre())
                    if main_window.curseur.deplacer_y(1):
                        main_window.set_case_principale_fenetre(0, 1)
                        main_window.afficher_carte_entiere()
                    else:
                        main_window.afficher_curseur()

                if event.key == pygame.K_LEFT:
                    main_window.rafraichir_case(main_window.curseur.get_position_x_fenetre(), main_window.curseur.get_position_y_fenetre())
                    if main_window.curseur.deplacer_x(-1):
                        main_window.set_case_principale_fenetre(-1, 0)
                        main_window.afficher_carte_entiere()
                    else:
                        main_window.afficher_curseur()

                if event.key == pygame.K_RIGHT:
                    main_window.rafraichir_case(main_window.curseur.get_position_x_fenetre(), main_window.curseur.get_position_y_fenetre())
                    if main_window.curseur.deplacer_x(1):
                        main_window.set_case_principale_fenetre(1, 0)
                        main_window.afficher_carte_entiere()
                    else:
                        main_window.afficher_curseur()

                if event.key == pygame.K_q:
                    if action.get_selected_tag():
                        print("deplacement")
                        action.deplacement()
                        main_window.afficher_carte_entiere()
                        action.selected = False
                    else:
                        print("selection")
                        action.selection_is_true()

                if event.key == pygame.K_b:
                    print("push B")
                    action.selected = False

